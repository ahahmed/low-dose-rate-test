-------------------------------------------------------------------------------
LDR test software for final thesis | SDU | Fall 2018|
Author: Ahmed Abdirashid Ahmed 

-------------------------------------------------------------------------------
Usage:

Setup BDAQ53 test system and make it work. 

Setup you your Ethernet connection with the FPGA by setting up a static ip:
	sudo ifconfig enp0s31f6 192.168.10.2

Try to ping the the FPGA:
	ping 192.168.10.16

If it does not work repaeat the steps a couple of times. 

--------------------------------------------------------------------------------

Create a new directory 
	mkdir projectdir
	
Navigate to projectdir
	cd projectdir  
	
Type these commands

	git clone -b master https://gitlab.cern.ch/ahahmed/ldr-test-software-final-thesis-sdu
	export PATH=$HOME/miniconda/bin:$PATH

	cd bdaq53

	python setup.py develop

	cd ..
You can now run the different test routines, or the supervisor. 
	
	python Supervisor.py
	
